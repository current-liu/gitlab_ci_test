#!/bin/sh
if [ "$1" = 'unittest' ]; then
    exec py.test --cov=sample/ tests/
fi

if [ "$1" = 'staticcheck' ]; then
    exec flake8 --ignore E501
    exec isort *.py */*.py -c
fi

if [ "$1" = 'bash' ]; then
    bash
fi

if [ "$1" = 'push_package' ]; then
    exec python setup.py sdist upload -r local
fi